package ictgradschool.industry.testingandrefactoring.ex02;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class RectangleTest {

    Rectangle rac = new Rectangle(10,10);
    @Test
    public void getArea(){
        assertEquals(100,rac.getArea(),1e-15);
    }
}
