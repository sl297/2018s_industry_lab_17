package ictgradschool.industry.testingandrefactoring.ex02;

import org.junit.Test;
import static org.junit.Assert.*;

public class CircleTest {
    Circle c = new Circle(10);

    @Test
    public void getArea(){
    assertEquals(314,c.getArea(),1e-15);
    }
}
