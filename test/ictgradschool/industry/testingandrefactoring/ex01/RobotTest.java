package ictgradschool.industry.testingandrefactoring.ex01;

import org.junit.Before;
import org.junit.Test;
import org.omg.CORBA.PUBLIC_MEMBER;

import static org.junit.Assert.*;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }



    @Test
    public void testDirectionEast(){
        assertEquals(Robot.Direction.North,myRobot.getDirection());
        myRobot.turn();
        assertEquals(Robot.Direction.East,myRobot.getDirection());
    }

    @Test
    public void testDirectionSouth(){
        assertEquals(Robot.Direction.East,myRobot.getDirection());
        myRobot.turn();
        assertEquals(Robot.Direction.South,myRobot.getDirection());
    }

    @Test
    public void testDirectionWest(){
        assertEquals(Robot.Direction.South,myRobot.getDirection());
        myRobot.turn();
        assertEquals(Robot.Direction.West,myRobot.getDirection());
    }

    @Test
    public void testDirectionNorth(){
        assertEquals(Robot.Direction.West,myRobot.getDirection());
        myRobot.turn();
        assertEquals(Robot.Direction.North,myRobot.getDirection());
    }


    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public  void testMoveEast(){
        boolean atRight = false;
        try {
            myRobot.turn();
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atRight = myRobot.currentState().column == 10;
            assertTrue(atRight);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().column);
        }
        }

        @Test
    public void testMove3(){
            try{
                assertFalse(10== myRobot.currentState().row);
                assertEquals(Robot.Direction.South,myRobot.getDirection());
                for(int i = 1;i<Robot.GRID_SIZE-1;i++){
                    myRobot.move();
                    assertEquals(10,myRobot.currentState().column);
                }
            } catch (IllegalMoveException e) {
                fail();
            }
            try{
                myRobot.move();
                fail();
            }catch (IllegalMoveException e){
                assertEquals(10,myRobot.currentState().row);
            }
        }

        @Test
    public void testMove4(){
        try{
            for(int i = 1;i<Robot.GRID_SIZE-1;i++){
                myRobot.move();
                assertEquals(1,myRobot.currentState().column);
            }
        } catch (IllegalMoveException e) {
            fail();
         }

        }

        @Test
    public void testMove5() {
            try{
                    myRobot.move();
                    myRobot.turn();
                    myRobot.move();
                    myRobot.turn();
                assertEquals(9,myRobot.currentState().row);
                assertEquals(2,myRobot.currentState().column);
                assertEquals(Robot.Direction.South,myRobot.getDirection());
            } catch (IllegalMoveException e) {
                fail();
            }
        }

        @Test
    public void testMove6(){
            try{
                    myRobot.turn();
                    myRobot.move();
                    myRobot.move();
                    myRobot.turn();
                assertEquals(3,myRobot.currentState().column);
                assertEquals(Robot.Direction.South,myRobot.getDirection());
                assertEquals(10,myRobot.currentState().row);
            }catch (IllegalMoveException e) {
                fail();
            }
        }

        @Test
    public void testMove7(){
            try{
                    myRobot.move();
                    myRobot.move();
                    myRobot.turn();
                    myRobot.move();
                assertEquals(8,myRobot.currentState().row);
                assertEquals(Robot.Direction.East,myRobot.getDirection());
                assertEquals(2,myRobot.currentState().column);
            }catch (IllegalMoveException e) {
                fail();
            }
        }

        @Test
    public void testMove8(){
            try{
                 myRobot.move();
                 myRobot.turn();
                 myRobot.turn();
                 myRobot.move();
                assertEquals(10,myRobot.currentState().row);
                assertEquals(Robot.Direction.South,myRobot.getDirection());
                assertEquals(1,myRobot.currentState().column);
            }catch (IllegalMoveException e) {
                fail();
            }


        }

}







