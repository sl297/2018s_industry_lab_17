package ictgradschool.industry.testingandrefactoring.ex02;

public class Rectangle{
    public  double width;
    public  double length;


    public Rectangle(double width, double length){
        this.width = width;
        this.length = length;
    }

    public double getArea() {
        return width*length;
    }
}