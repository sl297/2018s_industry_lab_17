package ictgradschool.industry.testingandrefactoring.ex02;

public class Circle {
    private int radius;

    public int getRadius(){
        return radius;
    }

    public Circle() {
        this((int)(Math.random()*100));
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    public double getArea(){
        return (int)(Math.PI*radius*radius);
    }
}
