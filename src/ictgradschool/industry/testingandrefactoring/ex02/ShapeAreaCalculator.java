package ictgradschool.industry.testingandrefactoring.ex02;

import ictgradschool.Keyboard;

import java.util.ArrayList;
import java.util.List;

public class ShapeAreaCalculator {
     Circle c = new Circle();

    public ShapeAreaCalculator() {

    }

    public void start() {
        System.out.println("Welcome to Shape Area Calculator!\n");
        System.out.println("Enter the width of the rectangle:");
        double rectWidth = Double.parseDouble(Keyboard.readInput());
        System.out.println("Enter the length of the rectangle:");
        double rectLength = Double.parseDouble(Keyboard.readInput());

        Rectangle r = new Rectangle(rectWidth, rectLength);
        System.out.println("");

        System.out.println("The radius of the circle is:" + c.getRadius());
        System.out.println("");

        int a= (int)r.getArea();
        int b = (int) c.getArea();
        int smaller = Math.min(a,b);

        System.out.println("The smaller area is: " + smaller);
    }

    public static void main(String[] args) {
        new ShapeAreaCalculator().start();
    }


}
